﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickField : MonoBehaviour {
public GameObject gameController;
public int fieldNr = 1;
Material material;
public Color highlightedBlue = new Color();
public Color highlightedRed = new Color();


	// Use this for initialization
	void Start () {
		material = GetComponent<Renderer>().material;
		// Color newCol;
		// if (ColorUtility.TryParseHtmlString("8300FF", out newCol))
		// {
   		// 	 highlightedBlue = newCol;
		// }
		// if (ColorUtility.TryParseHtmlString("FF8900", out newCol))
		// {
   		// 	 highlightedRed = newCol;
		// }
	}
	
	// Update is called once per frame
	void Update () {
		if(gameController.GetComponent<GameController>().resetField() == true){
			material.SetColor(Shader.PropertyToID("_Color"),Color.white);
			// gameController.GetComponent<GameController>().reset = false;
		}
	}

	private void OnMouseDown() {
		switch (gameController.GetComponent<GameController>().Check(fieldNr))
		{
			case 1: 
				material.SetColor(Shader.PropertyToID("_Color"),Color.blue);
				break;
			case 2: material.SetColor(Shader.PropertyToID("_Color"),Color.red);
				break;
			case 0:
				break;
		} 
	}

	// private void OnMouseOver()
	// {
	// 	switch (gameController.GetComponent<GameController>().Check(fieldNr))
	// 	{
	// 		case 1: 
	// 			material.SetColor(Shader.PropertyToID("_Color"),highlightedBlue);
	// 			break;
	// 		case 2: 
	// 			material.SetColor(Shader.PropertyToID("_Color"),highlightedRed);
	// 			break;
	// 		case 0:
	// 			break;
	// 	} 
	// }

}

